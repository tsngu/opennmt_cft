# openNMT
Projet sur openNMT pour la matière "Traduction automatique", M1 TAL INALCO.

# Script pour lemmatization 

Exemple de ligne de commande sous le format suivant : python nom_du_script.py fichier_a_lemma.ext_lang (fr/en) 

Pour le corpus Test Emea en français : python lemmatizer.py Emea_test_500.fr fr 
Pour le corpus Test Emea en anglais : python lemmatizer.py Emea_test_500.en en

