### Juste des notes pour ne pas oublier ce qui a été fait durant le projet.

Points à noter :
- Utilisation du GPU (NVIDIA), 16 GO de RAM.
- Traitement d'environ 20k tokens/s

## 3) Evaluation sur un corpus en formes fléchies

- Nous avons deux corpus initiaux un corpus Europarl (français-anglais) et un corpus Emea (français-anglais).
- Nettoyage des deux corpus par Fanny avec les étapes inscrites dans Guide_projet_OpenNMT.txt
- Découpage des corpus :
  - Europarl : TRAIN 100k phrases, DEV 3750 phrases, TEST 500 phrases
  - Emea : TRAIN 10k phrases, TEST 500 phrases. Pas de corpus DEV car on utilise celui d'Europarl.
    - Comme le corpus Emea est trop petit, on le concatène à la suite d'Europarl.
- Nous travaillons donc sur deux corpus : Europarl avec 100k de phrases et Europarl_Emea avec 110k, chacun comportant une version anglaise et une version française.
- Modification du yaml avec les bons chemins et le nombre de steps voulu.
  - exemple : [le yaml pour europarl](https://gitlab.com/tsngu/opennmt_cft/-/blob/main/europarl_100k.yaml)
- Nous construisons d'abord le vocabulaire avec la commande :
```bash
onmt_build_vocab -config europarl_100k.yaml -n_sample 100000
# YAML à changer selon le corpus
# -n_sample à 100 000 car nous avons 100k phrases
# donc pour le corpus Europarl_emea nous le changeons à 110 000.
```
Nous avons eu comme output :
![output vocab europarl](data/evaluation/output/screens/europarl/europarl_screen00.png)

- Ensuite nous entrons la commande : 
```bash
onmt_train -config europarl_100k.yaml
```
qui va output :
![output debut train europarl](data/evaluation/output/screens/europarl/europarl_screen01.png)
Quelques points :
- Bizarrement, la taille du vocabulaire change ? 
  - Passage de 31711 à 31720 pour la source et de 44242 à 32768 pour le target.
- Nous avons commencé avec 200k steps avec une vérification tous les 15k que j'ai ensuite changé à 10k.
- Nous voulons réduire le plus possible la perplexity et augmenter l'accuracy et pour ce faire, nous regardons surtout la "Validation perplexity" et la "Validation accuracy".

### Observations pour Europarl 100k :
(Ce serait cool si tu pouvais regarder les screens entre le début et la fin pour voir les différences entre les étapes et noter les majeures diff. Pareil pour le corpus Europarl_Emea)
-> J'ai regardé vite fait et :

A 70k de steps :
- Train perplexity : 6,227
- Train accuracy : 60,519
- Validation (dev) perplexity : 8.506
- Validation accuracy : 59.406
  
A 80k de steps :
- Train perplexity : 3.01
- Train accuracy : 72.02
- Validation perplexity : 8.6
- Validation accuracy : 59.46

On a un bond entre les deux. Puis ca staaaaagne. On arrive à monter à 73 de train accuracy à 130k steps mais ca reste comme ca jusqu'à 200k (voir screen du dessus) et les données de validations ont à peine bougé.
**Sauf que !!**

150k steps à l'air d'être le maximum :
![output 150k](data/evaluation/output/screens/europarl/europarl_screen17.png)
La validation perplexity est à 8.76079 et la validation accuracy est à 59.5889

Lorqu'on passe à 160k steps :
![output 160k](data/evaluation/output/screens/europarl/europarl_screen18.png)
On a un petit bond en validation perplexity et une diminution validation accuracy mais le train accuracy et le train perplexity évoluent dans le bon sens.

A la fin, nous obtenons :
![output final 200k](data/evaluation/output/screens/europarl/europarl_screen22.png)
Une validation accuracy à 59.585 et une validation perplexity à 8.765.

**Dilemme** : Est-ce que je traduis à partir de 150k et je jette mes 200k ? 

J'ai fait le test :
La traduction est lancée avec la commande :
```bash
onmt_translate -model data/evaluation/run/europarl/checkpoints/models_europarl_step_150000.pt -src data/evaluation/TEST/Europarl/Europarl_test_500.en -output data/evaluation/output/traductions/pred_europarl_100k_150.txt -gpu 0 -verbose

# puis

onmt_translate -model data/evaluation/run/europarl/checkpoints/models_europarl_step_200000.pt -src data/evaluation/TEST/Europarl/Europarl_test_500.en -output data/evaluation/output/traductions/pred_europarl_100k_200.txt -gpu 0 -verbose
```

et on vérifie le score bleu avec multi-bleu :
```bash
perl multi-bleu.perl -lc data/evaluation/TEST/Europarl/Europarl_test_500.fr < pred_europarl_100k_150.txt 

# puis

perl multi-bleu.perl -lc data/evaluation/TEST/Europarl/Europarl_test_500.fr < pred_europarl_100k_200.txt 
```

output :
![output score bleu](data/evaluation/output/screens/europarl/europarl_scorebleu.jpg)
Le score bleu qui utilise les 200k steps est meilleur.

**Conclusion** : Je vais continuer à run sur 200k steps pour voir les résultats.

### Tabelau des résultats.

|Modele|Score bleu|Train acc|Train ppl|Validation acc|Validation ppl|
|-----|-----|-----|-----|-----|-----|
|150k steps|27.71|73.1469|2.85258|59.5889|8.76079|
|200k steps|27.85|73.2361|2.84037|59.5854|8.76529|
-> Grande diff entre train et validation.

On répète la meme chose mais avec le corpus train Europarl_emea (+ dev Europarl).

### Observations pour Europarl_Emea 110k
- TRAIN EUROPARL_EMEA 110K
- TEST EMEA 500
- DEV EUROPARL 3750
- Même pb avec le vocab (voir screen 00 et screen 01 dans data/evaluation/output/screens/europarl_emea/)

Cette fois-ci, il atteint le "max" en validation acc et le "min" en validation ppl à 140k steps.
Ca commence à diminuer (pour l'acc) et augmenter (pour le ppl) à 150k 

On va traduire avec le modele de 140k steps et celui de 200k 
140k :
![output 140k](data/evaluation/output/screens/europarl_emea/europarl_emea_screen15.png)

200k :
![output 140k](data/evaluation/output/screens/europarl_emea/europarl_emea_screen21.png)

Le fichier de test = Emea 500.

```bash
onmt_translate -model data/evaluation/run/europarl_emea/checkpoints/models_europarl_emea_step_140000.pt -src data/evaluation/TEST/Emea/Emea_test_500.en -output data/evaluation/output/traductions/pred_europarl_emea_100k_140.txt -gpu 0 -verbose

# puis

onmt_translate -model data/evaluation/run/europarl_emea/checkpoints/models_europarl_emea_step_200000.pt -src data/evaluation/TEST/Emea/Emea_test_500.en -output data/evaluation/output/traductions/pred_europarl_emea_100k_200.txt -gpu 0 -verbose
```

et on vérifie le score bleu avec

```bash
perl multi-bleu.perl -lc data/evaluation/TEST/Emea/Emea_test_500.fr < data/evaluation/output/traductions/pred_europarl_emea_100k_140.txt

# puis

perl multi-bleu.perl -lc data/evaluation/TEST/Emea/Emea_test_500.fr < data/evaluation/output/traductions/pred_europarl_emea_100k_200.txt
```
output :
![resultats](data/evaluation/output/screens/europarl_emea/europarl_emea_scorebleu.png)

Cette fois ci, celui au corpus à 140k steps a un score legerement meilleur de 0,01 point.
=> Pas besoin d'aller jusqu'à 200k pour Emea.

|Modele|Score bleu|Train acc|Train ppl|Validation acc|Validation ppl|
|-----|-----|-----|-----|-----|-----|
|140k steps|26.64|67.1549|3.74065|65.6156|4.17621|
|200k steps|26.63|69.2233|3.34948|65.5915|4.17636|

-> Petite diff entre train et validation.

#### Resultats des deux corpus
||Europarl|Europarl_Emea|
|-----|-----|-----|
|140k steps|27.71|26.64|
|200k steps|27.85|26.63|


## 4) Avec les corpus lemmatisés.

Lemmatisation par Fanny.

### Corpus Europarl lemmatisé.

Construction du vocabulaire :

![output vocab](data/evaluation/output/screens/europarl_lemma/europarl_lemma_screen00.png)

Debut du train :
![début train](data/evaluation/output/screens/europarl_lemma/europarl_lemma_screen01.png)

Taille du vocab a aussi changé

Nous allons aussi prendre les modèles à 140k steps et 200k steps pour faire une comparaison.

![140k steps](data/evaluation/output/screens/europarl_lemma/europarl_lemma_screen15.png)

![200k steps](data/evaluation/output/screens/europarl_lemma/europarl_lemma_screen21.png)

Pour lancer la traduction :

```bash
onmt_translate -model data/evaluation/run/europarl_lemma/checkpoints/models_europarl_lemma_step_140000.pt -src data/evaluation/TEST/Europarl_lemma/Europarl_test_500.lemma.en -output data/evaluation/output/traductions/pred_europarl_lemma_100k_140.txt -gpu 0 -verbose

# puis

onmt_translate -model data/evaluation/run/europarl_lemma/checkpoints/models_europarl_lemma_step_200000.pt -src data/evaluation/TEST/Europarl_lemma/Europarl_test_500.lemma.en -output data/evaluation/output/traductions/pred_europarl_lemma_100k_200.txt -gpu 0 -verbose
```

et on vérifie le score bleu avec

```bash
perl multi-bleu.perl -lc data/evaluation/TEST/Europarl_lemma/Europarl_test_500.lemma.fr < data/evaluation/output/traductions/pred_europarl_lemma_100k_140.txt

# puis

perl multi-bleu.perl -lc data/evaluation/TEST/Europarl_lemma/Europarl_test_500.lemma.fr < data/evaluation/output/traductions/pred_europarl_lemma_100k_200.txt
```

output:
![score bleu euro lemma](data/evaluation/output/screens/europarl_lemma/europarl_lemma_scorebleu.png)

|Modele|Score bleu|Train acc|Train ppl|Validation acc|Validation ppl|
|-----|-----|-----|-----|-----|-----|
|140k steps|30.73|68.0454|4.05959|62.9245|7.25674|
|200k steps|30.67|69.7145|3.65639|62.9148|7.26052|

wow trop bg les scores bleus là (et 140k steps = mieux. 200k = overtrain i guess ?)

### Corpus Europarl_emea lemmatisé.

Construction du vocabulaire :

![output vocab](data/evaluation/output/screens/europarl_emea_lemma/europarl_emea_lemma_screen00.png)

Debut du train :
![début train](data/evaluation/output/screens/europarl_emea_lemma/europarl_emea_lemma_screen01.png)

Taille du vocab a aussi changé

Nous allons aussi prendre les modèles à 140k steps et 200k steps pour faire une comparaison.

![140k steps](data/evaluation/output/screens/europarl_emea_lemma/europarl_emea_lemma_screen15.png)

![200k steps](data/evaluation/output/screens/europarl_emea_lemma/europarl_emea_lemma_screen21.png)

Pour lancer la traduction (tjs fichier de test EMEA 500) :

```bash
onmt_translate -model data/evaluation/run/europarl_emea_lemma/checkpoints/models_europarl_emea_lemma_step_140000.pt -src data/evaluation/TEST/Emea_lemma/Emea_test_500.lemma.en -output data/evaluation/output/traductions/pred_europarl_emea_lemma_110k_140.txt -gpu 0 -verbose

# puis

onmt_translate -model data/evaluation/run/europarl_emea_lemma/checkpoints/models_europarl_emea_lemma_step_200000.pt -src data/evaluation/TEST/Emea_lemma/Emea_test_500.lemma.en -output data/evaluation/output/traductions/pred_europarl_emea_lemma_110k_200.txt -gpu 0 -verbose
```

et on vérifie le score bleu avec

```bash
perl multi-bleu.perl -lc data/evaluation/TEST/Emea_lemma/Emea_test_500.lemma.fr < data/evaluation/output/traductions/pred_europarl_emea_lemma_110k_140.txt

# puis

perl multi-bleu.perl -lc data/evaluation/TEST/Emea_lemma/Emea_test_500.lemma.fr < data/evaluation/output/traductions/pred_europarl_emea_lemma_110k_200.txt
```

output:
![score bleu euro lemma](data/evaluation/output/screens/europarl_emea_lemma/europarl_emea_lemma_scorebleu.png)

|Modele|Score bleu|Train acc|Train ppl|Validation acc|Validation ppl|
|-----|-----|-----|-----|-----|-----|
|140k steps|33.88|67.6782|4.1768|62.8008|7.20455|
|200k steps|33.91|69.3982|3.74655|62.783|7.205|

-> Malgré le fait qu'on ait un score de val plus bas, le score bleu est plus haut.

#### Résultats des deux corpus lemmatisés

||Europarl lemma|Europarl_Emea lemma|
|-----|-----|-----|
|140k steps|30.73|33.88|
|200k steps|30.67|33.91|

## Conclusion : résultat combiné des qutre corpus.


||Europarl|Europarl_Emea|Europarl lemma|Europarl_Emea lemma|
|-----|-----|-----|-----|-----|
|140k steps|27.71|26.64|30.73|33.88|
|200k steps|27.85|26.63|30.67|33.91|

**ccl** : cest mieux de lemmatiser, surtout si le test = un truc assez spécifique (emea)

