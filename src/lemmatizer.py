import spacy
import sys
import os
import time

def lemmatize_file(input_file, output_file, language):
    # Charger le modèle de langue Spacy en fonction de la langue spécifiée
    if language == "fr":
        nlp = spacy.load("fr_core_news_sm")
        lang_extension = ".fr"
    elif language == "en":
        nlp = spacy.load("en_core_web_sm")
        lang_extension = ".en"
    else:
        print("La langue spécifiée n'est pas prise en charge.")
        sys.exit(1)

    # Ouvrir le fichier d'entrée en lecture
    with open(input_file, "r", encoding="utf-8") as file:
        lines = file.readlines()

    # Lemmatisation du texte ligne par ligne
    lemmatized_lines = []
    start_time = time.time()
    total_lines = len(lines)
    processed_lines = 0

    for line in lines:
        line = line.strip()  # Supprimer les espaces en début et fin de ligne
        if line:  # Vérifier si la ligne n'est pas vide
            doc = nlp(line)
            lemmatized_line = " ".join([token.lemma_ for token in doc])
            lemmatized_lines.append(lemmatized_line)
        
        processed_lines += 1
        elapsed_time = time.time() - start_time
        avg_time_per_line = elapsed_time / processed_lines
        remaining_lines = total_lines - processed_lines
        estimated_time_remaining = remaining_lines * avg_time_per_line

        # Calculer la progression
        progress = processed_lines / total_lines
        progress_bar_width = 40
        completed_width = int(progress * progress_bar_width)
        remaining_width = progress_bar_width - completed_width

        # Affichage de la barre de chargement sur une seule ligne
        progress_bar = "[" + "=" * completed_width + ">" + " " * remaining_width + "]"
        progress_info = f"Progression: {processed_lines}/{total_lines}"
        time_info = f"Temps restant estimé: {estimated_time_remaining:.2f} secondes"
        print(f"\rTraitement en cours... {progress_bar} {progress_info} {time_info}", end="")

    # Obtenir le nom du fichier de sortie avec l'extension ".lemma" et l'extension de langue
    output_file = os.path.splitext(input_file)[0] + ".lemma" + lang_extension

    # Écrire le texte lemmatisé dans le fichier de sortie avec les divisions par retour à la ligne
    with open(output_file, "w", encoding="utf-8") as file:
        file.write("\n".join(lemmatized_lines))

    print("\nLemmatisation terminée. Résultat enregistré dans le fichier :", output_file)

# Vérifier si le nom du fichier d'entrée et la langue sont fournis en ligne de commande
if len(sys.argv) < 3:
    print("Veuillez spécifier le nom du fichier d'entrée et la langue (fr ou en) en ligne de commande.")
    sys.exit(1)

input_file = sys.argv[1]  # Récupérer le nom du fichier d'entrée à partir des arguments en ligne de commande
language = sys.argv[2]  # Récupérer la langue à partir des arguments en ligne de commande
output_file = None  # Définir la variable output_file

lemmatize_file(input_file, output_file, language)

