## Journal de bord.

### II) Experimentation 

#### Script

- Utilisation du GPU.
- Preprocessing des corpus avec les étapes inscrites dans Guide_projet_openNMT.txt"

Corpus à 10K :
```
## Where the samples will be written
save_data: resultats/vocab/europarl

## Where the vocab(s) will be written
src_vocab: resultats/vocab/europarl.vocab.src
tgt_vocab: resultats/vocab/europarl.vocab.tgt

# Prevent overwriting existing files in the folder
overwrite: False

# Corpus opts:
data:
    train:
        path_src: preproc/clean/Europarl_train_10k.tok.true.clean.en
        path_tgt: preproc/clean/Europarl_train_10k.tok.true.clean.fr
    valid:
        path_src: preproc/clean/Europarl_dev_1k.tok.true.clean.en
        path_tgt: preproc/clean/Europarl_dev_1k.tok.true.clean.fr

# Vocabulary files that were just created
src_vocab: resultats/vocab/europarl.vocab.src
tgt_vocab: resultats/vocab/europarl.vocab.tgt

# Train on a single GPU
world_size: 1
gpu_ranks: [0]

# Where to save the checkpoints
save_model: resultats/checkpoints/
save_checkpoint_steps: 500 # peut etre changé
train_steps: 5000 # peut etre changé
valid_steps: 2500 # peut etre changé
```

Commandes à exécuter directement dans le dossier "experimentation":
`onmt_build_vocab -config en_fr10k.yaml -n_sample 10000`

`onmt_train -config en_fr10k.yaml`

`onmt_translate -model resultats/checkpoints/_step_5000.pt -src preproc/clean/Europarl_test_500.tok.true.clean.en -output resultats/test10k5k -gpu 0 -verbose`

`perl ../multi-bleu.perl -lc preproc/clean/Europarl_test_500.tok.true.clean.fr < resultats/test10k5k`

#### Résultats :

Commencé avec 5k steps car je ne savais pas combien en mettre.

![5k steps](./data/experimentation/resultats/images/5ksteps.png)
![Score bleu 5k](./data/experimentation/resultats/images/5k_scorebleu.png)

Avec un score bleu de 10.56, j'essaie d'augmenter le nombre de train steps à 7k et 3k5 pour le valid steps.

![7k steps](./data/experimentation/resultats/images/7ksteps.png)
![Score bleu 7k](./data/experimentation/resultats/images/7k_scorebleu.png)

Le score bleu a augmenté à 11.06.
Prochain essai avec train_steps à 15k et 7k5 pour valid_steps.
-> Score bleu augmenté à 11.75. J'ai peur d'overtrain donc je laisse comme ça.

![15k steps](./data/experimentation/resultats/images/15ksteps.png)
![Score bleu 15k](./data/experimentation/resultats/images/15k_scorebleu.png)

### Evaluation sur des corpus parallèles en forme fléchies à large échelle


